package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Rent;
import ro.ubb.catalog.core.service.RentalService;
import ro.ubb.catalog.web.converter.RentalConverter;
import ro.ubb.catalog.web.dto.RentalDto;
import ro.ubb.catalog.web.dto.RentalsDto;
import ro.ubb.catalog.web.dto.EmptyJsonResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anca.
 */
@RestController
public class RentalController {
    public static final Logger log = LoggerFactory.getLogger(RentalController.class);

    @Autowired
    private RentalService rentalService;

    @Autowired
    private RentalConverter rentalConverter;


    @RequestMapping(value = "/rentals", method = RequestMethod.GET)
    public List<RentalDto> getRentals() {
        log.trace("getRentals");

        List<Rent> rentals = rentalService.getAllRentals();

        log.trace("getRentals: rentals={}", rentals);

        return new ArrayList<>(rentalConverter.convertModelsToDtos(rentals));
    }

    @RequestMapping(value = "/rentals", method = RequestMethod.POST)
    public RentalDto saveRental(@RequestBody RentalDto rentalDto) {
        log.trace("saveRental --- method entered");

        Rent savedRental = rentalService.saveRental(
                rentalDto.getIdMovie(),rentalDto.getIdClient(), rentalDto.getDate());
        RentalDto result = rentalConverter.convertModelToDto(savedRental);

        log.trace("saveRental: Rental saved -> result={}", result);
        return result;
    }

    @RequestMapping(value = "/rentals/{id}", method = RequestMethod.PUT)
    public RentalDto updateRental(
            @PathVariable final Long id,
            @RequestBody final RentalDto rentalDto) {
        log.trace("updateRental: id={}, RentalDtoMap={}", id, rentalDto);

        Rent rental = rentalService.updateRental(id,
                rentalDto.getIdMovie(),
                rentalDto.getIdClient(),
                rentalDto.getDate());

        RentalDto result = rentalConverter.convertModelToDto(rental);

        log.trace("updateRental: result={}", result);

        return result;
    }


    @RequestMapping(value = "/rentals/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteRental(@PathVariable Long id) {
        log.trace("deleteRental --- method entered");

        rentalService.deleteRental(id);

        log.trace("deleteRentalById --- rental deleted");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

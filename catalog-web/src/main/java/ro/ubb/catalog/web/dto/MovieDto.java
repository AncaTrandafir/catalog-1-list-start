package ro.ubb.catalog.web.dto;

import lombok.*;

/**
 * Created by anca.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper=false)

public class MovieDto extends BaseDto {
    private String year;
    private String title;
    private double price;

//    public MovieDto() {
//    }
//
//    public MovieDto(String year, String title, double price) {
//        this.year = year;
//        this.title = title;
//        this.price = price;
//    }
//
//    public String getYear() {
//        return year;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public double getPrice() {
//        return price;
//    }

    @Override
    public String toString() {
        return "MovieDto{" +
                "year='" + year + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                '}';
    }
}

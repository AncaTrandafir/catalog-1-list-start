package ro.ubb.catalog.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Rent;
import ro.ubb.catalog.web.dto.RentalDto;

/**
 * Created by anca.
 */
@Component
public class RentalConverter extends BaseConverter<Rent, RentalDto> {
    private static final Logger log = LoggerFactory.getLogger(RentalConverter.class);

    @Override
    public Rent convertDtoToModel(RentalDto dto) {
        Rent rental = new Rent(dto.getIdMovie(), dto.getIdClient(), dto.getDate());
        rental.setId(dto.getId());
        return rental;
    }

    @Override
    public RentalDto convertModelToDto(Rent rental) {
        RentalDto rentalDto = new RentalDto(rental.getIdMovie(), rental.getIdMovie(), rental.getDate());
        rentalDto.setId(rental.getId());
        return rentalDto;
    }
}

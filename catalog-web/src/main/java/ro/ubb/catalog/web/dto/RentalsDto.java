package ro.ubb.catalog.web.dto;

import lombok.*;

import java.util.Set;

/**
 * Created by anca.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class RentalsDto {
    private Set<RentalDto> rentals;
}

package ro.ubb.catalog.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.web.dto.MovieDto;

/**
 * Created by anca.
 */
@Component
public class MovieConverter extends BaseConverter<Movie, MovieDto> {
    private static final Logger log = LoggerFactory.getLogger(MovieConverter.class);

    @Override
    public Movie convertDtoToModel(MovieDto dto) {
        Movie movie = new Movie(dto.getYear(), dto.getTitle(), dto.getPrice());
        movie.setId(dto.getId());
        return movie;
    }

    @Override
    public MovieDto convertModelToDto(Movie movie) {
        MovieDto movieDto = new MovieDto(movie.getYear(), movie.getTitle(), movie.getPrice());
        movieDto.setId(movie.getId());
        return movieDto;
    }
}

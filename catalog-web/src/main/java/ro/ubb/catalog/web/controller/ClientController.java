package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.service.ClientService;
import ro.ubb.catalog.web.converter.ClientConverter;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.EmptyJsonResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anca.
 */
@RestController
public class ClientController {
    public static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientConverter clientConverter;


    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public List<ClientDto> getClients() {
        log.trace("getClients");

        List<Client> clients = clientService.getAllClients();

        log.trace("getClients: clients={}", clients);

        return new ArrayList<>(clientConverter.convertModelsToDtos(clients));
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    public ClientDto saveClient(@RequestBody ClientDto clientDto) {
        log.trace("saveClient --- method entered");

        Client savedClient = clientService.saveClient(
                clientDto.getFirstName(),clientDto.getLastName(), clientDto.getCNP(), clientDto.getAge());
        ClientDto result = clientConverter.convertModelToDto(savedClient);

        log.trace("saveClient: Client saved -> result={}", result);
        return result;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    public ClientDto updateClient(
            @PathVariable final Long id,
            @RequestBody final ClientDto clientDto) {
        log.trace("updateClient: id={}, clientDtoMap={}", id, clientDto);

        Client client = clientService.updateClient(id,
                clientDto.getFirstName(),
                clientDto.getLastName(),
                clientDto.getCNP(),
                clientDto.getAge());

        ClientDto result = clientConverter.convertModelToDto(client);

        log.trace("updateClient: result={}", result);

        return result;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
   public ResponseEntity<?> deleteClient(@PathVariable Long id) {
        log.trace("deleteClient --- method entered");

        clientService.deleteClient(id);

        log.trace("deleteClientById --- client deleted");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

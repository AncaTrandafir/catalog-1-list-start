package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.service.MovieService;
import ro.ubb.catalog.web.converter.MovieConverter;
import ro.ubb.catalog.web.dto.MovieDto;
import ro.ubb.catalog.web.dto.MoviesDto;
import ro.ubb.catalog.web.dto.EmptyJsonResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anca.
 */
@RestController
public class MovieController {
    public static final Logger log = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieConverter movieConverter;


    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    public List<MovieDto> getMovies() {
        log.trace("getMovies");

        List<Movie> movies = movieService.getAllMovies();

        log.trace("getMovies: movies={}", movies);

        return new ArrayList<>(movieConverter.convertModelsToDtos(movies));
    }

    @RequestMapping(value = "/movies", method = RequestMethod.POST)
   public MovieDto saveMovie(@RequestBody final MovieDto movieDto) {
        log.trace("saveMovie --- method entered");

        Movie savedMovie = movieService.saveMovie(
                movieDto.getYear(), movieDto.getTitle(), movieDto.getPrice());
        MovieDto result = movieConverter.convertModelToDto(savedMovie);

        log.trace("saveMovie: Movie saved -> result={}", result);
        return result;
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.PUT)
    public MovieDto updateMovie(
            @PathVariable final Long id,
            @RequestBody final MovieDto movieDto) {
        log.trace("updateMovie: id={}, movieDtoMap={}", id, movieDto);

        Movie movie = movieService.updateMovie(id,
                movieDto.getYear(),
                movieDto.getTitle(),
                movieDto.getPrice());

        MovieDto result = movieConverter.convertModelToDto(movie);

        log.trace("updateMovie: result={}", result);

        return result;
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteMovie(@PathVariable Long id) {
        log.trace("deleteMovie --- method entered");

        movieService.deleteMovie(id);

        log.trace("deleteMovieById --- movie deleted");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

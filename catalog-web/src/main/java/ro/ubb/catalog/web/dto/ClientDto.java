package ro.ubb.catalog.web.dto;

import lombok.*;

/**
 * Created by anca.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper=false)

public class ClientDto extends BaseDto {
    private String firstName;
    private String lastName;
    private String CNP;
    private int age;

//    public ClientDto() {
//    }
//
//    public ClientDto(String firstName, String lastName, String CNP, int age) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.CNP = CNP;
//        this. age = age;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public String getCNP() {
//        return CNP;
//    }
//
//    public int getAge() {
//        return age;
//    }

    @Override
    public String toString() {
        return "ClientDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", CNP='" + CNP + '\'' +
                ", age=" + age +
                '}';
    }
}

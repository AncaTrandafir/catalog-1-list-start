package ro.ubb.catalog.web.dto;

import lombok.*;

/**
 * Created by anca.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper=false)

public class RentalDto extends BaseDto {
    private Long idMovie;
    private Long idClient;
    private String date;

//    public RentalDto() {
//    }
//
//    public RentalDto(Long idMovie, Long idClient, String date) {
//        this.idMovie = idMovie;
//        this.idClient = idClient;
//        this.date= date;
//    }
//
//    public long getIdMovie() {
//        return idMovie;
//    }
//
//    public long getIdClient() {
//        return idClient;
//    }
//
//    public String getDate() {
//        return date;
//    }

    @Override
    public String toString() {
        return "RentalDto{" +
                "idMovie=" + idMovie +
                ", idClient=" + idClient +
                ", date='" + date + '\'' +
                '}';
    }
}

export class Movie {
    id: number;
    year: string;
    title: string;
    price: number;
}

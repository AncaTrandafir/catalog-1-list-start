import {Injectable} from '@angular/core';

import {HttpClient} from "@angular/common/http";

import {Rent} from "./rental.model";

import {Observable} from "rxjs";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class RentalService {
  private rentalsUrl = 'http://localhost:8080/api/rentals';

  constructor(private httpClient: HttpClient) {
  }

  getRentals(): Observable<Rent[]> {
    return this.httpClient
      .get<Array<Rent>>(this.rentalsUrl);
  }

  getRental(id: number): Observable<Rent> {
    return this.getRentals()
      .map(movies => movies.find(rental => rental.id === id));
  }

  update(rental): Observable<Rent> {
    const url = `${this.rentalsUrl}/${rental.id}`;
    return this.httpClient
      .put<Rent>(url, rental);
  }

  save(rental: Rent): Observable<Rent> {
    console.log(rental);
    return this.httpClient
      .post<Rent>(this.rentalsUrl, rental);
  }

  delete(id: number): Observable<any> {
    const url = `${this.rentalsUrl}/${id}`;
    console.log("delete url: ", url);
    return this.httpClient
      .delete(url);
  }

}

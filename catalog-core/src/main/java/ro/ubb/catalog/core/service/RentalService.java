package ro.ubb.catalog.core.service;


import ro.ubb.catalog.core.model.Rent;

import java.util.List;

/**
 * Created by anca.
 */
public interface RentalService {
    List<Rent> getAllRentals();

    Rent updateRental(Long id, Long idMovie, Long idClient, String date);

    void deleteRental(Long id);

    Rent saveRental(Long idMovie, Long idClient, String date);
}

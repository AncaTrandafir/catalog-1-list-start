package ro.ubb.catalog.core.service;


import ro.ubb.catalog.core.model.Movie;

import java.util.List;

/**
 * Created by anca.
 */
public interface MovieService {
    List<Movie> getAllMovies();

    Movie updateMovie(Long id, String year, String title, Double price);

    void deleteMovie(Long id);

    Movie saveMovie(String year, String title, double price);
}

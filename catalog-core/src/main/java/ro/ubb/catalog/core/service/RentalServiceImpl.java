package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Rent;
import ro.ubb.catalog.core.repository.RentalRepo;

import java.util.List;
import java.util.Optional;

/**
 * Created by anca.
 */
@Service
public class RentalServiceImpl implements RentalService {
    public static final Logger log = LoggerFactory.getLogger(RentalServiceImpl.class);

    @Autowired
    private RentalRepo rentalRepo;


    @Override
    public List<Rent> getAllRentals() {
        log.trace("getAllRentals --- method entered");

        List<Rent> result = rentalRepo.findAll();

        log.trace("getAllRentals: result={}", result);

        return result;
    }

    @Override
    public Rent saveRental(Long idMovie, Long idClient, String date) {
        log.trace("addRental: idMovie={}, idClient={}, date={}", idMovie, idClient, date);

        Rent rental = rentalRepo.save(new Rent(idMovie, idClient, date));

        log.trace("addRental: rental={}", rental);

        return rental;
    }

    @Transactional
    public Rent updateRental(Long id, Long idMovie, Long idClient, String date) {
        log.trace("updateRental --- method entered");
        Optional<Rent> rental = rentalRepo.findById(id);

        rental.ifPresent(r->{
            r.setIdMovie(idMovie);
            r.setIdClient(idClient);
        });

        log.trace("updateRental: update={}", rental.get());

        return rental.orElse(null);
    }

    @Override
    public void deleteRental(Long id) {
        rentalRepo.deleteById(id);

        log.trace("deleteRentalById --- rental deleted");
    }
}

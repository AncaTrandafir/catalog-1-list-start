package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;

/**
 * @author anca
 */

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)

public class Movie extends BaseEntity<Long> {
    private String year;
    private String title;
    private double price;

//    public Movie() {
//    }
//
//    public Movie(String year, String title, double price) {
//        this.year = year;
//        this.title = title;
//        this.price = price;
//    }
//
//    public String getYear() {
//        return year;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public double getPrice() {
//        return price;
//    }
//
//    public void setYear(String year) {
//        this.year = year;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public void setPrice(double price) {
//        this.price = price;
//    }

    @Override
    public String toString() {
        return "Movie{" +
                "year='" + year + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                '}';
    }
}

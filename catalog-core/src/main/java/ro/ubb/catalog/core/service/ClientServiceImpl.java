package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.repository.ClientRepo;

import java.util.List;
import java.util.Optional;

/**
 * Created by anca.
 */
@Service
public class ClientServiceImpl implements ClientService {
    public static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepo clientRepo;

    @Override
    public List<Client> getAllClients() {
        log.trace("getAllClients --- method entered");

        List<Client> result = clientRepo.findAll();

        log.trace("getAllClients: result={}", result);

        return result;
    }

    @Override
    public Client saveClient(String firstName, String lastName, String CNP, int age) {
        log.trace("addClient: firstName={}, lastName={}, CNP={}, age={}", firstName, lastName, CNP, age);

        Client client = clientRepo.save(new Client(firstName, lastName, CNP, age));

        log.trace("addClient: client={}", client);

        return client;
    }

    @Override
    @Transactional
    public Client updateClient(Long id, String firstName, String lastName, String CNP, int age) {
        log.trace("updateClient --- method entered");
        Optional<Client> client = clientRepo.findById(id);

        client.ifPresent(c->{
            c.setFirstName(firstName);
            c.setLastName(lastName);
            c.setCNP(CNP);
            c.setAge(age);
        });

        log.trace("updateClient: update={}", client.get());

        return client.orElse(null);
    }

    @Override
    public void deleteClient(Long id) {
        clientRepo.deleteById(id);

        log.trace("deleteClientById --- client deleted");
    }
}

//package ro.ubb.catalog.core.model;
//
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//
///**
// * @author anca
// */
//
//public class MovieValidator implements Validator<Movie> {
//
//    @Autowired
//    private Movie movie;
//
//    @Override
//    public void validate(Movie entity) throws ValidatorException {
//        SimpleDateFormat format = new SimpleDateFormat("yyyy");
//        try {
//            format.parse(entity.getYear());
//        } catch (ParseException pe) {
//            throw new RuntimeException("The year of release is not in a correct format!");
//        }
//
//        double price;
//
//        if (entity.getPrice() <= 0) {
//            throw new RuntimeException("Price must be a positive number");
//        }
//
//    }
//}

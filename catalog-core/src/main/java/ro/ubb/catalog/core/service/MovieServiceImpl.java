package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.repository.MovieRepo;

import java.util.List;
import java.util.Optional;

/**
 * Created by anca.
 */
@Service
public class MovieServiceImpl implements MovieService {
    public static final Logger log = LoggerFactory.getLogger(MovieServiceImpl.class);

    @Autowired
    private MovieRepo movieRepo;


    @Override
    public List<Movie> getAllMovies() {
        log.trace("getAllMovies --- method entered");

        List<Movie> result = movieRepo.findAll();

        log.trace("getAllMovies: result={}", result);

        return result;
    }

    @Override
    public Movie saveMovie(String year, String title, double price) {
        log.trace("addMovie: year={}, title={}, price={}", year, title, price);

        Movie movie = movieRepo.save(new Movie(year, title, price));

        log.trace("addMovie: movie={}", movie);

        return movie;
    }

    @Override
    @Transactional
    public Movie updateMovie(Long id, String year, String title, Double price) {
        log.trace("updateMovie --- method entered");
        Optional<Movie> movie = movieRepo.findById(id);

        movie.ifPresent(m->{
            m.setYear(year);
            m.setTitle(title);
            m.setPrice(price);
        });

        log.trace("updateMovie: update={}", movie.get());

        return movie.orElse(null);
    }

    @Override
    public void deleteMovie(Long id) {
        movieRepo.deleteById(id);

        log.trace("deleteMovieById --- movie deleted");
    }
}

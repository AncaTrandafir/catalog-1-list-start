package ro.ubb.catalog.core.repository;

import ro.ubb.catalog.core.model.Movie;

public interface MovieRepo
        extends GenericRepo<Movie, Long> {
}

package ro.ubb.catalog.core.repository;

import ro.ubb.catalog.core.model.Client;

public interface ClientRepo
        extends GenericRepo <Client, Long> {
}

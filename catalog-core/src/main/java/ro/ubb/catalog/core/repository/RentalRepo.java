package ro.ubb.catalog.core.repository;

import ro.ubb.catalog.core.model.Rent;

public interface RentalRepo
        extends GenericRepo<Rent, Long> {
}

package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)

public class Rent extends BaseEntity<Long> {

    private Long idMovie;
    private Long idClient;
    private String date;

//    public Rent() {
//    }
//
//    public Rent(Long idMovie, Long idClient, String date) {
//        this.idMovie = idMovie;
//        this.idClient = idClient;
//        this.date= date;
//    }
//
//    public long getIdMovie() {
//        return idMovie;
//    }
//
//    public long getIdClient() {
//        return idClient;
//    }
//
//    public String getDate() {
//        return date;
//    }
//
//    public void setIdMovie(long idMovie) {
//        this.idMovie = idMovie;
//    }
//
//    public void setIdClient(long idClient) {
//        this.idClient = idClient;
//    }
//
//    public void setDate(String date) {
//        this.date = date;
//    }

    @Override
    public String toString() {
        return "Rent{" +
                "idMovie=" + idMovie +
                ", idClient=" + idClient +
                ", date='" + date + '\'' +
                '}';
    }
}



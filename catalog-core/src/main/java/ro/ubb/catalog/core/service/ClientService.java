package ro.ubb.catalog.core.service;


import ro.ubb.catalog.core.model.Client;

import java.util.List;

/**
 * Created by anca.
 */
public interface ClientService {
    List<Client> getAllClients();

    Client updateClient(Long id, String firstName, String lastName, String CNP, int age);

    void deleteClient(Long id);

    Client saveClient(String firstName, String lastName, String cnp, int age);
}

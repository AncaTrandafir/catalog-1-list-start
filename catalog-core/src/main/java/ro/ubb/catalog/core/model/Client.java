package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)

public class Client extends BaseEntity<Long> {

    private String firstName;
    private String lastName;
    private String CNP;
    private int age;

//    public Client() {
//    }
//
//    public Client(String firstName, String lastName, String CNP, int age) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.CNP = CNP;
//        this. age = age;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public String getCNP() {
//        return CNP;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public void setCNP(String CNP) {
//        this.CNP = CNP;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }

    @Override
    public String toString() {
        return "Client{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", CNP='" + CNP + '\'' +
                ", age=" + age +
                '}';
    }
}

// call super = true afiseaza id
